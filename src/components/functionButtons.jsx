import React, { Component } from 'react';
import PropTypes from 'prop-types';

import FunctionalButton from './functionalButton';

export default class FunctionButtons extends Component {
  constructor(props) {
    super(props);

    this.showMonthesTable = this.showMonthesTable.bind(this);
    this.showYearsTable = this.showYearsTable.bind(this);
  }

  showMonthesTable(e) {
    this.props.showMonthsTable();
  }
  
  showYearsTable() {
    this.props.showYearsTable();
  }

  render() {
    return (
      <div className="funcs">
        <FunctionalButton 
          title={this.props.month} 
          className="month" 
          onClick={this.showMonthesTable}
        />
        <FunctionalButton 
          title={this.props.year} 
          className="year" 
          onClick={this.showYearsTable}
        />
      </div>
    )
  }
}

FunctionButtons.propTypes = {
  month: PropTypes.string.isRequired,
  year: PropTypes.number.isRequired,
  showMonthsTable: PropTypes.func.isRequired,
  showYearsTable: PropTypes.func.isRequired
}
