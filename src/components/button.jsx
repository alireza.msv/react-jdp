import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'class-names';

export default class Button extends Component {
  constructor(props) {
    super(props);

    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  handleButtonClick(e) {
    e.preventDefault();

    if (typeof this.props.onClick === 'function')
      this.props.onClick();
  }

  render() {
    return (
      <button className={`button ${this.props.className}`} onClick={this.handleButtonClick}>
        {this.props.children}
      </button>
    )
  }
}
