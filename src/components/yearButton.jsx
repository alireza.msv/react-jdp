import React, { Component } from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

export default class YearButton extends Component {
  constructor(props) {
    super(props);

    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  handleButtonClick(e) {
    e.stopPropagation();

    if (typeof this.props.onClick === 'function')
      this.props.onClick(this.props.year);
  }

  render() {
    return (
      <span className={classNames({
        'button': true,
        'year-btn': true,
        'current': this.props.current,
        'disabled': this.props.disabled
      })} onClick={this.handleButtonClick}>
        {this.props.children}
      </span>
    )
  }
}

YearButton.propTypes = {
  current: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func
}

YearButton.defaultProps = {
  current: false,
  disabled: false
}
