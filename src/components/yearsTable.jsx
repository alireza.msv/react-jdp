import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'class-names';

import YearButton from './yearButton';

export default class YearsTable extends Component {
  constructor(props) {
    super(props);

    this.renderYears = this.renderYears.bind(this);
    this.hideYearsTable = this.hideYearsTable.bind(this);
    this.handleNextYearsPage = this.handleNextYearsPage.bind(this);
    this.handlePrevYearsPage = this.handlePrevYearsPage.bind(this);
  }

  renderYears() {
    return this.props.yearsList.map(y => (
      <YearButton
        key={y.year}
        {...y}
        onClick={this.props.changeYear}
      >
        {y.year}
      </YearButton>
    ))

    for(let i=0; i<4; i++) {
      let ySpans = [];
      for(let j=0; j<4; j++) {
        let year = this.props.yearsList[j + i*4]

        ySpans.push(
          <YearButton key={j} {...year} onClick={this.props.changeYear}>{year.year}</YearButton>
        )
      }

      yearRows.push(
        <div className="year-row" key={i}>
          {ySpans}
        </div>
      )
    }

    return yearRows;
  }

  hideYearsTable(e) {
    e.stopPropagation();
    this.props.hideYearsTable()
  }

  handleStopPropagation(e) {
    e.stopPropagation();
  }

  handleNextYearsPage(e) {
    e.stopPropagation();
    this.props.nextYearsPage();
  }

  handlePrevYearsPage(e) {
    e.stopPropagation();
    this.props.prevYearsPage();
  }

  render() {
    return (
      <div className={classNames({
        'year-select': true,
        'in': this.props.visible
      })} onClick={this.hideYearsTable}>
        <div className="year-pagination prev" onClick={this.handlePrevYearsPage}></div>
        <div className="years-container">
          {this.renderYears()}
        </div>
        <div className="year-pagination next" onClick={this.handleNextYearsPage}></div>
      </div>
    )
  }
}

YearsTable.propTypes = {
  yearsList: PropTypes.array.isRequired,
  visible: PropTypes.bool,
  hideYearsTable: PropTypes.func.isRequired,
  changeYear: PropTypes.func.isRequired,
  nextYearsPage: PropTypes.func.isRequired,
  prevYearsPage: PropTypes.func.isRequired
}

YearsTable.defaultProps = {
  visible: false
}
