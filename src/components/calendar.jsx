import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'class-names';

import DayButton from './dayButton';

export default class Calendar extends Component {
  constructor(props) {
    super(props);

    this.mapMonthDays = this.mapMonthDays.bind(this);
  }

  mapMonthDays() {
    return this.props.monthDays.map((day, i) => (
      <DayButton 
        key={i} 
        {...day} 
        pickDay={this.props.pickDay}
        highlightWeekends={this.props.highlightWeekends}
      />
  ))
  }

  render() {
    return (
      <div className="days">
        {this.mapMonthDays()}
      </div>
    );
  }
}

Calendar.propTypes = {
  monthDays: PropTypes.array.isRequired
}
