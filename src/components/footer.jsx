import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'class-names';

import * as types from '../constants/calendarTypes';
import * as sizes from '../constants/sizes';
import * as shapes from '../constants/shapes';

import Button from './button';

export default class Footer extends Component {
  constructor(props) {
    super(props);

    this.todayToString = this.todayToString.bind(this);
    this.handlePickToday = this.handlePickToday.bind(this);
  }

  todayToString() {
    let { year, monthName, day} = this.props.today;

    let prefix = this.props.type === types.JALAALI ? 'امروز ' : 'today';

    if (this.props.size === sizes.SMALL && this.props.shape === shapes.SQUARE)
      prefix = '';

    return `${prefix}${day} ${monthName} ${year}`;
  }

  handlePickToday() {
    const { day, month, year } = this.props.today;

    this.props.pickDay(year, month, day);
  }

  render() {
    return (
      <div className="footer-btns">
        <div className="col right">
          <Button className="button-today" onClick={this.handlePickToday}>{this.todayToString()}</Button>
        </div>
        <div className="col left">
          <Button className="button-toggle" onClick={this.props.toggleCalendarType}>{this.props.type === types.JALAALI ? 'جلالی' : 'میلادی'}</Button>
        </div>
      </div>
    )
  }
}

Footer.propTypes = {
  today: PropTypes.object.isRequired,
  pickDay: PropTypes.func.isRequired,
  toggleCalendarType: PropTypes.func.isRequired
}
