import React, { Component } from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

export default class MonthButton extends Component {
  constructor(props) {
    super(props);

    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  handleButtonClick(e) {
    e.stopPropagation();
    if (!this.props.disabled && this.props.onClick)
      this.props.onClick(this.props.index + 1)
  }

  render() {
    return (
      <span className={classNames({
        'button': true,
        'month-btn': true,
        'disabled': this.props.disabled,
        'current': this.props.current
      })} onClick={this.handleButtonClick}>
        {this.props.name}
      </span>
    )
  }
}

MonthButton.propTypes = {
  name: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  current: PropTypes.bool,
  onClick: PropTypes.func
}

MonthButton.defaultProps = {
  disabled: false,
  current: false
}
