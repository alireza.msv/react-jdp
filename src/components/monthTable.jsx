import React, { Component } from 'react';
import classNames from 'class-names';
import PropTypes from 'prop-types';

import MonthButton from './monthButton';

export default class MonthTable extends Component {
  constructor(props) {
    super(props);

    this.renderMonths = this.renderMonths.bind(this);
    this.hideMonthTable = this.hideMonthTable.bind(this);
    this.changeMonth = this.changeMonth.bind(this);
  }

  hideMonthTable(e) {
    e.stopPropagation()
    this.props.hideMonthTable();
  }

  changeMonth(month) {
    this.props.changeMonth(month);
  }

  renderMonths() {
    let seasonDivs = [];
    for (let i=0; i<4; i++) {
      let monthCells = [];
      for (let j=0; j<3; j++) {
        let month = this.props.monthList[j + i*3];

        monthCells.push(
          <MonthButton {...month} index={j + i*3} key={j} onClick={this.changeMonth}/>
        )
      }
      seasonDivs.push(
        <div className="season" key={seasonDivs.length}>
          {monthCells}
        </div>
      )
    }

    return seasonDivs;
  }

  render() {
    return (
      <div className={classNames({
        'month-select': true,
        'in': this.props.visible
      })} onClick={this.hideMonthTable}>
        <div className="months-container">
          {this.renderMonths()}
        </div>
      </div>
    )
  }
}

MonthTable.propTypes = {
  monthList: PropTypes.array.isRequired,
  hideMonthTable: PropTypes.func.isRequired
}
