import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class WeekHeader extends Component {
  constructor(props) {
    super(props);

    this.renderWeekDayNames = this.renderWeekDayNames.bind(this);
  }

  renderWeekDayNames() {
    return this.props.daysName.map((day, i) => {
      return <div key={i} className="cell">{day}</div>
    });
  }

  render() {
    return (
      <div className="head">
      {this.renderWeekDayNames()}
      </div>
    )
  }
}

WeekHeader.defaultProps = {
  daysName: []
}

WeekHeader.propTypes = {
  daysName: PropTypes.array.isRequired
}
