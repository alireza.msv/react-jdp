import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'class-names';

export default class DayButton extends Component {
  constructor(props) {
    super(props);

    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  handleButtonClick(e) {
    e.stopPropagation();
    
    if (!this.props.disabled)
      this.props.pickDay(this.props.year, this.props.month, this.props.day);
  }

  render() {
    return (
      <div className={classNames({
        'cell': true,
        'fade': true,
        'in': true,
        'current': this.props.current,
        // 'disabled': this.props.nextMonth || this.props.prevMonth,
        'next': this.props.nextMonth,
        'prev': this.props.prevMonth,
        'today': this.props.today,
        'weekend': this.props.highlightWeekends && this.props.dow === 6
      })} onClick={this.handleButtonClick}>
        {this.props.day}
      </div>
    )
  }
}

DayButton.propTypes = {
  day: PropTypes.number.isRequired,
  pickDay: PropTypes.func.isRequired,
  next: PropTypes.bool,
  prev: PropTypes.bool,
  disabled: PropTypes.bool,
  current: PropTypes.bool,
  today: PropTypes.bool
}
