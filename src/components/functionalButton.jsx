import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class FunctionalButton extends Component {
  constructor(props) {
    super(props);

    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  handleButtonClick(e) {
    e.preventDefault();

    if (typeof this.props.onClick === 'function')
      this.props.onClick();
  }

  render() {
    return (
      <div 
        className={this.props.className}
        onClick={this.handleButtonClick}
      >
        {this.props.title}
      </div>
    )
  }
}

FunctionalButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  title: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.number.isRequired
  ]),
  className: PropTypes.string,
}
