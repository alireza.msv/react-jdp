import React, { Component } from 'react';
import classNames from 'class-names';
import propTypes from 'prop-types';
import { Portal } from 'react-portal';
import Transition from 'react-transition-group/Transition';

import * as sizes from './constants/sizes';
import * as placements from './constants/palcements';
import * as types from './constants/calendarTypes';
import { jalaaliMonths, gregorianMonths } from './constants/calendar';

import FunctionButtons from './components/functionButtons';
import MonthTable from './components/monthTable';
import YearsTable from './components/yearsTable';
import WeekHeader from './components/weekHeader';
import Calendar from './components/calendar';
import Footer from './components/footer';

import { mapJalaaliDayNames, mapGregorianDayNames, mapJalaaliMonthToCalendar, mapGregorianMonthToCalendar, mapJalaaliMonths, mapGregorianMonths, mapYears, getGregorianDate, getJalaaliDate, gregorianMonthLength, dateToString } from './utils/dateUtil';
import { mapPropsToOptions, mapOptionsToClassNames, toggleOptions, calculatePlacement } from './utils/jdpUtil';

export default class Jdp extends Component {
  constructor (props) {
    super(props);

    const options = mapPropsToOptions(props);
    const monthTable = mapJalaaliMonthToCalendar(options.today.year, options.today.month);

    this.state = {
      options,
      isMonthsVisible: false,
      isYearsVisible: false,
      offset: {top: 0, left: 0},
      visible: false,
    };

    this.handleWindowClick = this.handleWindowClick.bind(this);
    this.showMonthsTable = this.showMonthsTable.bind(this);
    this.hideMonthTable = this.hideMonthTable.bind(this);
    this.showYearsTable = this.showYearsTable.bind(this);
    this.hideYearsTable = this.hideYearsTable.bind(this);
    this.changeMonth = this.changeMonth.bind(this);
    this.changeYear = this.changeYear.bind(this);
    this.toggleCalendarType = this.toggleCalendarType.bind(this);
    this.nextYearsPage = this.nextYearsPage.bind(this);
    this.prevYearsPage = this.prevYearsPage.bind(this);
    this.pickDay = this.pickDay.bind(this);
    this.hide = this.hide.bind(this);
    this.pick = this.pick.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.show && !this.props.show) {
      setTimeout(() => {
        const { parent } = nextProps;
  
        let { top, left, arrowLeft, arrowTop, placement } = calculatePlacement(parent, this.jdp, this.state.options.placement);
        
        this.jdp.style.left = left + 'px';
        this.jdp.style.top = top + 'px';
  
        if (arrowLeft)
          this.jdp.querySelector('.arrow').style.left = arrowLeft + 'px'
  
        if (arrowTop)
          this.jdp.querySelector('.arrow').style.top = arrowTop + 'px'

        this.setState({
          visible: true,
          currentPlacement: placement
        })
      }, 50)
    } else if (!nextProps.show)
      this.setState({
        visible: false
      })
  }

  componentDidMount() {
    window.addEventListener('click', this.handleWindowClick)
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.handleWindowClick)
  }

  handleWindowClick(e) {
    this.hide()
  }

  handleJDPClick(e) {
    e.stopPropagation()
  }

  showMonthsTable() {
    this.setState({
      isMonthsVisible: true
    })
  }

  hideMonthTable() {
    this.setState({
      isMonthsVisible: false
    })
  }

  showYearsTable() {
    this.setState({
      isYearsVisible: true
    })
  }

  hideYearsTable() {
    this.setState({
      isYearsVisible: false,
      options: {
        ...this.state.options,
        yearsPage: 0
      }
    })
  }

  changeMonth(month) {
    this.setState({
      options: {
        ...this.state.options,
        month
      },
    });

    this.hideMonthTable();
  }

  changeYear(year) {
    this.setState({
      isYearsVisible: false,
      options: {
        ...this.state.options,
        year,
        yearsPage: 0
      }
    });
  }

  nextYearsPage() {
    let { yearsPage } = this.state.options;
    yearsPage ++;

    this.setState({
      options: {
        ...this.state.options,
        yearsPage
      }
    })
  }

  prevYearsPage() {
    let { yearsPage } = this.state.options;
    yearsPage --;

    this.setState({
      options: {
        ...this.state.options,
        yearsPage
      }
    })
  }

  toggleCalendarType() {
    this.setState({
      options: toggleOptions(this.state.options)
    })
  }

  pickDay(year, month, day) {
    this.setState({
      options: {
        ...this.state.options,
        current: {
          day,
          month,
          year
        }
      }
    })

    this.pick(year, month, day);
  }

  pick(year, month, day) {
    const { format } = this.props;

    if (this.state.options.type === types.JALAALI) {
      const gregDate = getGregorianDate(year, month, day)

      this.props.onPick({
        jalaali: {
          year, month, day
        }, 
        gregorian: {
          year: gregDate.year,
          month: gregDate.month,
          day: gregDate.day
        },
        jalaaliString: dateToString(year, month, day, format),
        gregorianString: dateToString(gregDate.year, gregDate.month, gregDate.day, format)
      })
    } else {
      const jalDate = getJalaaliDate(year, month, day)

      this.props.onPick({
        jalaali: {
          year: jalDate.year,
          month: jalDate.month,
          day: jalDate.day
        },
        gregorian: {
          year, month, day
        },
        jalaaliString: dateToString(jalDate.year, jalDate.month, jalDate.day, format),
        gregorianString: dateToString(year, month, day, format)
      })
    }

    this.hide()
  }

  hide() {
    this.setState({
      visible: false
    })

    setTimeout(() => {
      if (this.props.onHide)
        this.props.onHide();
    }, 300)
  }

  render() {
    const jdpClasses = mapOptionsToClassNames(this.state.options);
    const { type, month, year, day, current } = this.state.options;
    const monthName = (type === types.JALAALI) ? jalaaliMonths[month -1] : gregorianMonths[month -1];
    const monthDays = (type === types.JALAALI) ? mapJalaaliMonthToCalendar(year, month, {current}) : mapGregorianMonthToCalendar(year, month, {current});
    const { currentPlacement } = this.state;

    if (!this.props.show)
      return null

    return (
      <Transition
        appear={true}
        in={this.props.show}
        timeout={100}
      >
        { state => (
          <Portal node={document.body}>
            <div 
              className={classNames({
                [jdpClasses]: true,
                'in': state === 'entered' && this.state.visible,
                'top': currentPlacement === placements.TOP,
                'right': currentPlacement === placements.RIGHT,
                'bottom': currentPlacement === placements.BOTTOM,
                'left': currentPlacement === placements.LEFT
              })} 
              ref={ref => {this.jdp = ref}}
              onClick={this.handleJDPClick}
            >
              <div className="arrow"></div>
              <div className="content">
                <FunctionButtons
                  month={monthName}
                  year={year}
                  showMonthsTable={this.showMonthsTable}
                  showYearsTable={this.showYearsTable}
                  />
                <WeekHeader daysName={type === types.JALAALI ? mapJalaaliDayNames() : mapGregorianDayNames()}/>
                <Calendar
                  monthDays={monthDays}
                  pickDay={this.pickDay}
                  highlightWeekends={this.props.highlightWeekends}
                  />
                <Footer
                  today={this.state.options.today}
                  type={this.state.options.type}
                  toggleCalendarType={this.toggleCalendarType}
                  pickDay={this.pickDay}
                  size={this.state.options.size}
                  shape={this.state.options.shape}
                />
                <MonthTable
                  monthList={type === types.JALAALI ? mapJalaaliMonths(month) : mapGregorianMonths(month)}
                  visible={this.state.isMonthsVisible}
                  hideMonthTable={this.hideMonthTable}
                  changeMonth={this.changeMonth}
                />
                <YearsTable
                  yearsList={mapYears(year, this.state.options.yearsPage)}
                  visible={this.state.isYearsVisible}
                  hideYearsTable={this.hideYearsTable}
                  changeYear={this.changeYear}
                  nextYearsPage={this.nextYearsPage}
                  prevYearsPage={this.prevYearsPage}
                />
              </div>
            </div>
          </Portal>
          )
        }
      </Transition>
    )
  }
}

Jdp.propTypes = {
  show: propTypes.bool.isRequired,
  parent: propTypes.object,
  onHide: propTypes.func.isRequired,
  onPick: propTypes.func.isRequired,
  type: propTypes.oneOf(['jalaali', 'gregorian']),
  size: propTypes.oneOf(['small', 'medium', 'large']),
  placement: propTypes.oneOf(['top', 'right', 'bottom', 'left']),
  animated: propTypes.bool,
  year: propTypes.number,
  month: propTypes.number,
  day: propTypes.number,
  current: propTypes.object,
  format: propTypes.string,
  highlightWeekends: propTypes.bool,
  shape: propTypes.oneOf(['rectangle', 'square'])
}

Jdp.defaultProps = {
  type: 'jalaali',
  size: 'medium',
  placement: 'bottom',
  animated: true,
  format: 'yyyy/mm/dd',
  highlightWeekends: true,
  shape: 'rectangle'
}