import { toJalaali, toGregorian, jalaaliMonthLength } from 'jalaali-js';

import { jalaaliMonths, jalaaliWeekDays, gregorianMonths, gregorianWeekDays } from '../constants/calendar'

export const jalaaliToday = () => {
  const tdate = new Date();

  return getJalaaliDate(tdate.getFullYear(), tdate.getMonth() + 1, tdate.getDate());
}

export const getJalaaliDayOfWeek = (day) => ((day + 2) > 6) ? (day + 1) % 7 : day + 1

export const getJalaaliDate = (year, month, day) => {
  const date = new Date(year, month, day);
  const { jy, jm, jd } = toJalaali(year, month, day);
  const dow = getJalaaliDayOfWeek(date.getDay());

  return {
    year: jy,
    month: jm,
    day: jd,
    monthName: jalaaliMonths[jm -1],
    dow,
    dowName: jalaaliWeekDays[dow]
  }
}

export const gregorianToday = () => {
  const gDate = new Date();
  const year = gDate.getFullYear();
  const month = gDate.getMonth() + 1;
  const day = gDate.getDate();
  const dow = gDate.getDay();
  const monthName = gregorianMonths[month - 1];

  return {
    year,
    month,
    day,
    dow,
    monthName
  }
}

export const getGregorianDate = (year, month, day) => {
  const { gy, gm, gd } = toGregorian(year, month, day);
  const date = new Date(gy, gm -1, gd);
  const dow = date.getDay();

  return {
    year: gy,
    month: gm,
    day: gd,
    monthName: gregorianMonths[month -1],
    dow,
    dowName: gregorianWeekDays[dow]
  }
}

export const mapJalaaliDayNames = () => {
  return jalaaliWeekDays.map((day) => {
    return day[0];
  })
}

export const mapGregorianDayNames = () => {
  return gregorianWeekDays.map((day) => {
    return day.substring(0, 3);
  })
}

export const mapJalaaliMonthToCalendar = (year, month, options = {current: {year: 0, month: 0, day: 0}}) => {
  const monthFirst = getGregorianDate(year, month, 1);
  const monthFirstDow = getJalaaliDayOfWeek(monthFirst.dow);
  const monthLength = jalaaliMonthLength(year, month);
  const today = jalaaliToday();
  const { current } = options;

  let days = [];

  if (monthFirstDow > 0) {
    let prevMonthLength;
    if (month === 1)
      prevMonthLength = jalaaliMonthLength(year -1, 12);
    else
      prevMonthLength = jalaaliMonthLength(year, month -1);

    for(let i=0;i<monthFirstDow; i++) {
      days.push({
        month: (month > 1) ? month - 1 : 12,
        year: (month > 1) ? year : year - 1,
        day: prevMonthLength - monthFirstDow + i + 1,
        prevMonth: true,
        dow: (monthFirstDow - i) % 7,
      })
    }
  }

  for(let i=0; i<monthLength; i++)
    days.push({
      month,
      year,
      day: i + 1,
      current: (year === current.year && month === current.month && (i + 1) === current.day),
      today: (year === today.year && month === today.month && (i + 1) === today.day),
      dow: (monthFirstDow + i) % 7
    })

  if (days.length % 7) {
    if (month === 12) {
      month++
      year++;
    }

    for(let i=0; i< days.length % 7; i++)
      days.push({
        day: i + 1,
        month: month < 12 ? month + 1 : 1,
        year: month < 12 ? year : year + 1,
        nextMonth: true,
        dow: days[days.length -1].dow + 1
      })
  }

  return days;
}

export const mapGregorianMonthToCalendar = (year, month, options = {current: {year: 0, month: 0, day: 0}}) => {
  const monthFirst = new Date(year, month - 1,1);
  const monthFirstDow = monthFirst.getDay();
  const monthLength = gregorianMonthLength(year, month);
  const { current } = options;
  const today = gregorianToday();

  let days = [];

  if (monthFirstDow > 0) {
    let prevMonthLength;
    if (month === 1)
      prevMonthLength = gregorianMonthLength(year -1, 12);
    else
      prevMonthLength = gregorianMonthLength(year, month -1);

    for(let i=0;i<monthFirstDow; i++) {
      days.push({
        month: (month > 1) ? month - 1 : 12,
        year: (month > 1) ? year : year - 1,
        day: prevMonthLength - monthFirstDow + i + 1,
        prevMonth: true,
        dow: (monthFirstDow - i) % 7
      })
    }
  }

  for (let i=0; i<monthLength; i++)
    days.push({
      year: year,
      month: month,
      day: i + 1,
      current: (year === current.year && month === current.month && (i + 1) === current.day),
      today: (year === today.year && month === today.month && (i + 1) === today.day),
      dow: (i + monthFirstDow) % 7
    })

  if (days.length % 7)
    for (let i=0; i<days.length % 7; i++)
      days.push({
        day: i + 1,
        month: month < 12 ? month + 1 : 1,
        year: month < 12 ? year : year + 1,
        nextMonth: true,
        dow: days[days.length - 1].dow + 1
      })
  return days;
}

export const mapJalaaliMonths = (current) => {
  return jalaaliMonths.map((month, index) => {
    return {
      name: month,
      index,
      disabled: false,
      current: current === index + 1
    }
  });
}

export const mapGregorianMonths = (current) => {
  return gregorianMonths.map((month, index) => ({
    name: month,
    index,
    disabled: false,
    current: current === index + 1
  }))
}

export const mapYears = (year, yearspage) => {
  const yearsPerPage = 16;
  const startYear = year + (yearsPerPage * yearspage) - 13;
  let years = [];

  for (let i=0;i<yearsPerPage; i++) {
    years.push({
      year: startYear + i,
      disabled: false,
      current: year === startYear + i
    });
  }

  return years;
}

export const gregorianMonthLength = (year, month) => {
  if (month === 1 || month === 3 || month === 5 || month === 7 || month === 8 || month === 10 || month === 12)
    return 31;

  if (month === 4 || month === 6 || month === 9 || month === 11)
    return 30;

  if (year % 4)
    return 28;
  else
    return 29;
}

export const dateToString = (year, month, day, format) => {
  if (typeof format === 'undefined')
    format = 'yyyy/mm/dd'

  let monthStr = month < 10 ? '0' + month.toString() : month.toString()
  let dayStr = day < 10 ? '0' + day.toString() : day.toString()

  let formatted = '';

  if (format.indexOf('yyyy') >= 0)
    formatted = format.replace('yyyy', year)
  else if (format.indexOf('yy') >= 0)
    formatted = format.replace('yy', year.toString().substring(2))

  if (format.indexOf('mm') >= 0)
    formatted = formatted.replace('mm', monthStr)
  else if (format.indexOf('m') >= 0)
    formatted = formatted.replace('m', month)
  
  if (format.indexOf('dd') >= 0)
    formatted = formatted.replace('dd', dayStr)
  else if (format.indexOf('d') >= 0)
    formatted = formatted.replace('d', day)
  
  return formatted
}