import classNames from 'class-names';

import * as types from '../constants/calendarTypes';
import * as sizes from '../constants/sizes';
import * as placements from '../constants/palcements';
import * as shapes from '../constants/shapes';

import { jalaaliToday, getJalaaliDate, gregorianToday, getGregorianDate } from './dateUtil';

export const mapPropsToOptions = (props) => {
  let { type, size, placement, animated, current, visible, shape } = props;

  if (type === undefined)
    type = types.JALAALI;
  else
    type = type.toUpperCase();

  if (size === undefined)
    size = sizes.MEDIUM;
  else
    size = size.toUpperCase();

  if (placement === undefined)
    placement = placements.BOTTOM;
  else
    placement = placement.toUpperCase();

  if (animated === undefined)
    animated = true;

  let year, month, day, today;

  if (type === types.JALAALI)
    today = jalaaliToday();
  else
    today = gregorianToday();

  if (year === undefined)
    year = today.year;

  if (month === undefined)
    month = today.month;

  if (day === undefined)
    day = today.day;
  
  const yearsPage = 0;

  if (current === undefined)
    current = {
      year: 0,
      month: 0,
      day: 0
    }

  return {
    type,
    size,
    placement,
    today,
    year,
    month,
    day,
    animated,
    yearsPage,
    current,
    visible,
    shape
  }
}

export const mapOptionsToClassNames = options => {
  return classNames({
    'jdp': true,
    'jalaali': options.type === types.JALAALI,
    'gregorian': options.type === types.GREGORIAN,
    'animated': options.animated,
    'sm': options.size === sizes.SMALL,
    'md': options.size === sizes.MEDIUM,
    'lg': options.size === sizes.LARGE,
    'rectangle': options.shape === shapes.RECTANGLE,
    'square': options.shape === shapes.SQUARE
  })
}

export const toggleOptions = options => {
  let { type, today, year, month, day } = options;

  if (type === types.JALAALI) {
    type = types.GREGORIAN;
    today = gregorianToday();
    let gDate = getGregorianDate(year, month, day);
    year = gDate.year;
    month = gDate.month;
    day = gDate.day;
  } else {
    type = types.JALAALI;
    today = jalaaliToday();
    let jDate = getJalaaliDate(year, month, day);
    year = jDate.year;
    month = jDate.month;
    day = jDate.day;
  }

  return {
    ...options,
    type,
    today,
    year,
    month,
    day
  }
}

export const calculatePlacement = (el, jdp, srcPlacement) => {
  let elCR = el.getBoundingClientRect();
  const elTop = elCR.top;
  const elLeft = elCR.left;

  const elWidth = el.offsetWidth;
  const elHeight = el.offsetHeight;
  const jdpHeight = jdp.offsetHeight;
  const jdpWidth = jdp.offsetWidth;
  const scrollY = window.scrollY;
  const scrollX = window.scrollX;

  let left = elLeft;
  let top = elTop;
  let arrowLeft, arrowTop;
  let placement = srcPlacement;

  if (placement === placements.BOTTOM)
    top += el.offsetHeight + scrollY;
  else if (placement === placements.TOP) {
    top -= jdpHeight + scrollY;

    if (top < jdpHeight)
      return calculatePlacement(el, jdp, placements.BOTTOM)
  } else if (placement === placements.RIGHT || placement === placements.LEFT) {
    top -= (jdpHeight - elHeight) / 2

    if (top < 0) {
      top = 5;
      arrowTop = elTop + (elHeight / 2) - 5;
    }
  }

  if (placement === placements.TOP || placement === placements.BOTTOM) {
    left -= ((jdpWidth - elWidth) / 2) + scrollX
    
    if (left < 0) {
      left = 10;
      arrowLeft = (elWidth / 2) - 10;
    } else if (left > window.innerWidth - jdpWidth) {
      left = window.innerWidth - jdpWidth - 10 + scrollX;
      arrowLeft = (elLeft - left) + (elWidth / 2)
    }
  } else if (placement === placements.RIGHT) {
    left += elWidth + scrollX;

    if (left + jdpWidth > window.innerWidth)
      return calculatePlacement(el, jdp, placement.BOTTOM)
  } else if (placement === placement.LEFT) {
    left -= jdpWidth + scrollX;

    if (left < 0)
      return calculatePlacement(el, jdp, placement.BOTTOM)
  }

  return {
    left, 
    top,
    arrowLeft,
    arrowTop,
    placement
  }
}