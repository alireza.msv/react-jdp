'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.calculatePlacement = exports.toggleOptions = exports.mapOptionsToClassNames = exports.mapPropsToOptions = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _classNames = require('class-names');

var _classNames2 = _interopRequireDefault(_classNames);

var _calendarTypes = require('../constants/calendarTypes');

var types = _interopRequireWildcard(_calendarTypes);

var _sizes = require('../constants/sizes');

var sizes = _interopRequireWildcard(_sizes);

var _palcements = require('../constants/palcements');

var placements = _interopRequireWildcard(_palcements);

var _shapes = require('../constants/shapes');

var shapes = _interopRequireWildcard(_shapes);

var _dateUtil = require('./dateUtil');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapPropsToOptions = exports.mapPropsToOptions = function mapPropsToOptions(props) {
  var type = props.type,
      size = props.size,
      placement = props.placement,
      animated = props.animated,
      current = props.current,
      visible = props.visible,
      shape = props.shape;


  if (type === undefined) type = types.JALAALI;else type = type.toUpperCase();

  if (size === undefined) size = sizes.MEDIUM;else size = size.toUpperCase();

  if (placement === undefined) placement = placements.BOTTOM;else placement = placement.toUpperCase();

  if (animated === undefined) animated = true;

  var year = void 0,
      month = void 0,
      day = void 0,
      today = void 0;

  if (type === types.JALAALI) today = (0, _dateUtil.jalaaliToday)();else today = (0, _dateUtil.gregorianToday)();

  if (year === undefined) year = today.year;

  if (month === undefined) month = today.month;

  if (day === undefined) day = today.day;

  var yearsPage = 0;

  if (current === undefined) current = {
    year: 0,
    month: 0,
    day: 0
  };

  return {
    type: type,
    size: size,
    placement: placement,
    today: today,
    year: year,
    month: month,
    day: day,
    animated: animated,
    yearsPage: yearsPage,
    current: current,
    visible: visible,
    shape: shape
  };
};

var mapOptionsToClassNames = exports.mapOptionsToClassNames = function mapOptionsToClassNames(options) {
  return (0, _classNames2.default)({
    'jdp': true,
    'jalaali': options.type === types.JALAALI,
    'gregorian': options.type === types.GREGORIAN,
    'animated': options.animated,
    'sm': options.size === sizes.SMALL,
    'md': options.size === sizes.MEDIUM,
    'lg': options.size === sizes.LARGE,
    'rectangle': options.shape === shapes.RECTANGLE,
    'square': options.shape === shapes.SQUARE
  });
};

var toggleOptions = exports.toggleOptions = function toggleOptions(options) {
  var type = options.type,
      today = options.today,
      year = options.year,
      month = options.month,
      day = options.day;


  if (type === types.JALAALI) {
    type = types.GREGORIAN;
    today = (0, _dateUtil.gregorianToday)();
    var gDate = (0, _dateUtil.getGregorianDate)(year, month, day);
    year = gDate.year;
    month = gDate.month;
    day = gDate.day;
  } else {
    type = types.JALAALI;
    today = (0, _dateUtil.jalaaliToday)();
    var jDate = (0, _dateUtil.getJalaaliDate)(year, month, day);
    year = jDate.year;
    month = jDate.month;
    day = jDate.day;
  }

  return _extends({}, options, {
    type: type,
    today: today,
    year: year,
    month: month,
    day: day
  });
};

var calculatePlacement = exports.calculatePlacement = function calculatePlacement(el, jdp, srcPlacement) {
  var elCR = el.getBoundingClientRect();
  var elTop = elCR.top;
  var elLeft = elCR.left;

  var elWidth = el.offsetWidth;
  var elHeight = el.offsetHeight;
  var jdpHeight = jdp.offsetHeight;
  var jdpWidth = jdp.offsetWidth;
  var scrollY = window.scrollY;
  var scrollX = window.scrollX;

  var left = elLeft;
  var top = elTop;
  var arrowLeft = void 0,
      arrowTop = void 0;
  var placement = srcPlacement;

  if (placement === placements.BOTTOM) top += el.offsetHeight + scrollY;else if (placement === placements.TOP) {
    top -= jdpHeight + scrollY;

    if (top < jdpHeight) return calculatePlacement(el, jdp, placements.BOTTOM);
  } else if (placement === placements.RIGHT || placement === placements.LEFT) {
    top -= (jdpHeight - elHeight) / 2;

    if (top < 0) {
      top = 5;
      arrowTop = elTop + elHeight / 2 - 5;
    }
  }

  if (placement === placements.TOP || placement === placements.BOTTOM) {
    left -= (jdpWidth - elWidth) / 2 + scrollX;

    if (left < 0) {
      left = 10;
      arrowLeft = elWidth / 2 - 10;
    } else if (left > window.innerWidth - jdpWidth) {
      left = window.innerWidth - jdpWidth - 10 + scrollX;
      arrowLeft = elLeft - left + elWidth / 2;
    }
  } else if (placement === placements.RIGHT) {
    left += elWidth + scrollX;

    if (left + jdpWidth > window.innerWidth) return calculatePlacement(el, jdp, placement.BOTTOM);
  } else if (placement === placement.LEFT) {
    left -= jdpWidth + scrollX;

    if (left < 0) return calculatePlacement(el, jdp, placement.BOTTOM);
  }

  return {
    left: left,
    top: top,
    arrowLeft: arrowLeft,
    arrowTop: arrowTop,
    placement: placement
  };
};