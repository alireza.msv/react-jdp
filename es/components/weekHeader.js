'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var WeekHeader = function (_Component) {
  _inherits(WeekHeader, _Component);

  function WeekHeader(props) {
    _classCallCheck(this, WeekHeader);

    var _this = _possibleConstructorReturn(this, (WeekHeader.__proto__ || Object.getPrototypeOf(WeekHeader)).call(this, props));

    _this.renderWeekDayNames = _this.renderWeekDayNames.bind(_this);
    return _this;
  }

  _createClass(WeekHeader, [{
    key: 'renderWeekDayNames',
    value: function renderWeekDayNames() {
      return this.props.daysName.map(function (day, i) {
        return _react2.default.createElement(
          'div',
          { key: i, className: 'cell' },
          day
        );
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'head' },
        this.renderWeekDayNames()
      );
    }
  }]);

  return WeekHeader;
}(_react.Component);

exports.default = WeekHeader;


WeekHeader.defaultProps = {
  daysName: []
};

WeekHeader.propTypes = {
  daysName: _propTypes2.default.array.isRequired
};