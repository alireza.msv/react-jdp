'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classNames = require('class-names');

var _classNames2 = _interopRequireDefault(_classNames);

var _yearButton = require('./yearButton');

var _yearButton2 = _interopRequireDefault(_yearButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var YearsTable = function (_Component) {
  _inherits(YearsTable, _Component);

  function YearsTable(props) {
    _classCallCheck(this, YearsTable);

    var _this = _possibleConstructorReturn(this, (YearsTable.__proto__ || Object.getPrototypeOf(YearsTable)).call(this, props));

    _this.renderYears = _this.renderYears.bind(_this);
    _this.hideYearsTable = _this.hideYearsTable.bind(_this);
    _this.handleNextYearsPage = _this.handleNextYearsPage.bind(_this);
    _this.handlePrevYearsPage = _this.handlePrevYearsPage.bind(_this);
    return _this;
  }

  _createClass(YearsTable, [{
    key: 'renderYears',
    value: function renderYears() {
      var _this2 = this;

      return this.props.yearsList.map(function (y) {
        return _react2.default.createElement(
          _yearButton2.default,
          _extends({
            key: y.year
          }, y, {
            onClick: _this2.props.changeYear
          }),
          y.year
        );
      });

      for (var i = 0; i < 4; i++) {
        var ySpans = [];
        for (var j = 0; j < 4; j++) {
          var year = this.props.yearsList[j + i * 4];

          ySpans.push(_react2.default.createElement(
            _yearButton2.default,
            _extends({ key: j }, year, { onClick: this.props.changeYear }),
            year.year
          ));
        }

        yearRows.push(_react2.default.createElement(
          'div',
          { className: 'year-row', key: i },
          ySpans
        ));
      }

      return yearRows;
    }
  }, {
    key: 'hideYearsTable',
    value: function hideYearsTable(e) {
      e.stopPropagation();
      this.props.hideYearsTable();
    }
  }, {
    key: 'handleStopPropagation',
    value: function handleStopPropagation(e) {
      e.stopPropagation();
    }
  }, {
    key: 'handleNextYearsPage',
    value: function handleNextYearsPage(e) {
      e.stopPropagation();
      this.props.nextYearsPage();
    }
  }, {
    key: 'handlePrevYearsPage',
    value: function handlePrevYearsPage(e) {
      e.stopPropagation();
      this.props.prevYearsPage();
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: (0, _classNames2.default)({
            'year-select': true,
            'in': this.props.visible
          }), onClick: this.hideYearsTable },
        _react2.default.createElement('div', { className: 'year-pagination prev', onClick: this.handlePrevYearsPage }),
        _react2.default.createElement(
          'div',
          { className: 'years-container' },
          this.renderYears()
        ),
        _react2.default.createElement('div', { className: 'year-pagination next', onClick: this.handleNextYearsPage })
      );
    }
  }]);

  return YearsTable;
}(_react.Component);

exports.default = YearsTable;


YearsTable.propTypes = {
  yearsList: _propTypes2.default.array.isRequired,
  visible: _propTypes2.default.bool,
  hideYearsTable: _propTypes2.default.func.isRequired,
  changeYear: _propTypes2.default.func.isRequired,
  nextYearsPage: _propTypes2.default.func.isRequired,
  prevYearsPage: _propTypes2.default.func.isRequired
};

YearsTable.defaultProps = {
  visible: false
};