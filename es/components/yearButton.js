'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classNames = require('class-names');

var _classNames2 = _interopRequireDefault(_classNames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var YearButton = function (_Component) {
  _inherits(YearButton, _Component);

  function YearButton(props) {
    _classCallCheck(this, YearButton);

    var _this = _possibleConstructorReturn(this, (YearButton.__proto__ || Object.getPrototypeOf(YearButton)).call(this, props));

    _this.handleButtonClick = _this.handleButtonClick.bind(_this);
    return _this;
  }

  _createClass(YearButton, [{
    key: 'handleButtonClick',
    value: function handleButtonClick(e) {
      e.stopPropagation();

      if (typeof this.props.onClick === 'function') this.props.onClick(this.props.year);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'span',
        { className: (0, _classNames2.default)({
            'button': true,
            'year-btn': true,
            'current': this.props.current,
            'disabled': this.props.disabled
          }), onClick: this.handleButtonClick },
        this.props.children
      );
    }
  }]);

  return YearButton;
}(_react.Component);

exports.default = YearButton;


YearButton.propTypes = {
  current: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  onClick: _propTypes2.default.func
};

YearButton.defaultProps = {
  current: false,
  disabled: false
};