'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _functionalButton = require('./functionalButton');

var _functionalButton2 = _interopRequireDefault(_functionalButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FunctionButtons = function (_Component) {
  _inherits(FunctionButtons, _Component);

  function FunctionButtons(props) {
    _classCallCheck(this, FunctionButtons);

    var _this = _possibleConstructorReturn(this, (FunctionButtons.__proto__ || Object.getPrototypeOf(FunctionButtons)).call(this, props));

    _this.showMonthesTable = _this.showMonthesTable.bind(_this);
    _this.showYearsTable = _this.showYearsTable.bind(_this);
    return _this;
  }

  _createClass(FunctionButtons, [{
    key: 'showMonthesTable',
    value: function showMonthesTable(e) {
      this.props.showMonthsTable();
    }
  }, {
    key: 'showYearsTable',
    value: function showYearsTable() {
      this.props.showYearsTable();
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'funcs' },
        _react2.default.createElement(_functionalButton2.default, {
          title: this.props.month,
          className: 'month',
          onClick: this.showMonthesTable
        }),
        _react2.default.createElement(_functionalButton2.default, {
          title: this.props.year,
          className: 'year',
          onClick: this.showYearsTable
        })
      );
    }
  }]);

  return FunctionButtons;
}(_react.Component);

exports.default = FunctionButtons;


FunctionButtons.propTypes = {
  month: _propTypes2.default.string.isRequired,
  year: _propTypes2.default.number.isRequired,
  showMonthsTable: _propTypes2.default.func.isRequired,
  showYearsTable: _propTypes2.default.func.isRequired
};