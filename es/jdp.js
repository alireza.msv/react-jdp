'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classNames2 = require('class-names');

var _classNames3 = _interopRequireDefault(_classNames2);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactPortal = require('react-portal');

var _Transition = require('react-transition-group/Transition');

var _Transition2 = _interopRequireDefault(_Transition);

var _sizes = require('./constants/sizes');

var sizes = _interopRequireWildcard(_sizes);

var _palcements = require('./constants/palcements');

var placements = _interopRequireWildcard(_palcements);

var _calendarTypes = require('./constants/calendarTypes');

var types = _interopRequireWildcard(_calendarTypes);

var _calendar = require('./constants/calendar');

var _functionButtons = require('./components/functionButtons');

var _functionButtons2 = _interopRequireDefault(_functionButtons);

var _monthTable = require('./components/monthTable');

var _monthTable2 = _interopRequireDefault(_monthTable);

var _yearsTable = require('./components/yearsTable');

var _yearsTable2 = _interopRequireDefault(_yearsTable);

var _weekHeader = require('./components/weekHeader');

var _weekHeader2 = _interopRequireDefault(_weekHeader);

var _calendar2 = require('./components/calendar');

var _calendar3 = _interopRequireDefault(_calendar2);

var _footer = require('./components/footer');

var _footer2 = _interopRequireDefault(_footer);

var _dateUtil = require('./utils/dateUtil');

var _jdpUtil = require('./utils/jdpUtil');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Jdp = function (_Component) {
  _inherits(Jdp, _Component);

  function Jdp(props) {
    _classCallCheck(this, Jdp);

    var _this = _possibleConstructorReturn(this, (Jdp.__proto__ || Object.getPrototypeOf(Jdp)).call(this, props));

    var options = (0, _jdpUtil.mapPropsToOptions)(props);
    var monthTable = (0, _dateUtil.mapJalaaliMonthToCalendar)(options.today.year, options.today.month);

    _this.state = {
      options: options,
      isMonthsVisible: false,
      isYearsVisible: false,
      offset: { top: 0, left: 0 },
      visible: false
    };

    _this.handleWindowClick = _this.handleWindowClick.bind(_this);
    _this.showMonthsTable = _this.showMonthsTable.bind(_this);
    _this.hideMonthTable = _this.hideMonthTable.bind(_this);
    _this.showYearsTable = _this.showYearsTable.bind(_this);
    _this.hideYearsTable = _this.hideYearsTable.bind(_this);
    _this.changeMonth = _this.changeMonth.bind(_this);
    _this.changeYear = _this.changeYear.bind(_this);
    _this.toggleCalendarType = _this.toggleCalendarType.bind(_this);
    _this.nextYearsPage = _this.nextYearsPage.bind(_this);
    _this.prevYearsPage = _this.prevYearsPage.bind(_this);
    _this.pickDay = _this.pickDay.bind(_this);
    _this.hide = _this.hide.bind(_this);
    _this.pick = _this.pick.bind(_this);
    return _this;
  }

  _createClass(Jdp, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var _this2 = this;

      if (nextProps.show && !this.props.show) {
        setTimeout(function () {
          var parent = nextProps.parent;

          var _calculatePlacement = (0, _jdpUtil.calculatePlacement)(parent, _this2.jdp, _this2.state.options.placement),
              top = _calculatePlacement.top,
              left = _calculatePlacement.left,
              arrowLeft = _calculatePlacement.arrowLeft,
              arrowTop = _calculatePlacement.arrowTop,
              placement = _calculatePlacement.placement;

          _this2.jdp.style.left = left + 'px';
          _this2.jdp.style.top = top + 'px';

          if (arrowLeft) _this2.jdp.querySelector('.arrow').style.left = arrowLeft + 'px';

          if (arrowTop) _this2.jdp.querySelector('.arrow').style.top = arrowTop + 'px';

          _this2.setState({
            visible: true,
            currentPlacement: placement
          });
        }, 50);
      } else if (!nextProps.show) this.setState({
        visible: false
      });
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      window.addEventListener('click', this.handleWindowClick);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('click', this.handleWindowClick);
    }
  }, {
    key: 'handleWindowClick',
    value: function handleWindowClick(e) {
      this.hide();
    }
  }, {
    key: 'handleJDPClick',
    value: function handleJDPClick(e) {
      e.stopPropagation();
    }
  }, {
    key: 'showMonthsTable',
    value: function showMonthsTable() {
      this.setState({
        isMonthsVisible: true
      });
    }
  }, {
    key: 'hideMonthTable',
    value: function hideMonthTable() {
      this.setState({
        isMonthsVisible: false
      });
    }
  }, {
    key: 'showYearsTable',
    value: function showYearsTable() {
      this.setState({
        isYearsVisible: true
      });
    }
  }, {
    key: 'hideYearsTable',
    value: function hideYearsTable() {
      this.setState({
        isYearsVisible: false,
        options: _extends({}, this.state.options, {
          yearsPage: 0
        })
      });
    }
  }, {
    key: 'changeMonth',
    value: function changeMonth(month) {
      this.setState({
        options: _extends({}, this.state.options, {
          month: month
        })
      });

      this.hideMonthTable();
    }
  }, {
    key: 'changeYear',
    value: function changeYear(year) {
      this.setState({
        isYearsVisible: false,
        options: _extends({}, this.state.options, {
          year: year,
          yearsPage: 0
        })
      });
    }
  }, {
    key: 'nextYearsPage',
    value: function nextYearsPage() {
      var yearsPage = this.state.options.yearsPage;

      yearsPage++;

      this.setState({
        options: _extends({}, this.state.options, {
          yearsPage: yearsPage
        })
      });
    }
  }, {
    key: 'prevYearsPage',
    value: function prevYearsPage() {
      var yearsPage = this.state.options.yearsPage;

      yearsPage--;

      this.setState({
        options: _extends({}, this.state.options, {
          yearsPage: yearsPage
        })
      });
    }
  }, {
    key: 'toggleCalendarType',
    value: function toggleCalendarType() {
      this.setState({
        options: (0, _jdpUtil.toggleOptions)(this.state.options)
      });
    }
  }, {
    key: 'pickDay',
    value: function pickDay(year, month, day) {
      this.setState({
        options: _extends({}, this.state.options, {
          current: {
            day: day,
            month: month,
            year: year
          }
        })
      });

      this.pick(year, month, day);
    }
  }, {
    key: 'pick',
    value: function pick(year, month, day) {
      var format = this.props.format;


      if (this.state.options.type === types.JALAALI) {
        var gregDate = (0, _dateUtil.getGregorianDate)(year, month, day);

        this.props.onPick({
          jalaali: {
            year: year, month: month, day: day
          },
          gregorian: {
            year: gregDate.year,
            month: gregDate.month,
            day: gregDate.day
          },
          jalaaliString: (0, _dateUtil.dateToString)(year, month, day, format),
          gregorianString: (0, _dateUtil.dateToString)(gregDate.year, gregDate.month, gregDate.day, format)
        });
      } else {
        var jalDate = (0, _dateUtil.getJalaaliDate)(year, month, day);

        this.props.onPick({
          jalaali: {
            year: jalDate.year,
            month: jalDate.month,
            day: jalDate.day
          },
          gregorian: {
            year: year, month: month, day: day
          },
          jalaaliString: (0, _dateUtil.dateToString)(jalDate.year, jalDate.month, jalDate.day, format),
          gregorianString: (0, _dateUtil.dateToString)(year, month, day, format)
        });
      }

      this.hide();
    }
  }, {
    key: 'hide',
    value: function hide() {
      var _this3 = this;

      this.setState({
        visible: false
      });

      setTimeout(function () {
        if (_this3.props.onHide) _this3.props.onHide();
      }, 300);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      var jdpClasses = (0, _jdpUtil.mapOptionsToClassNames)(this.state.options);
      var _state$options = this.state.options,
          type = _state$options.type,
          month = _state$options.month,
          year = _state$options.year,
          day = _state$options.day,
          current = _state$options.current;

      var monthName = type === types.JALAALI ? _calendar.jalaaliMonths[month - 1] : _calendar.gregorianMonths[month - 1];
      var monthDays = type === types.JALAALI ? (0, _dateUtil.mapJalaaliMonthToCalendar)(year, month, { current: current }) : (0, _dateUtil.mapGregorianMonthToCalendar)(year, month, { current: current });
      var currentPlacement = this.state.currentPlacement;


      if (!this.props.show) return null;

      return _react2.default.createElement(
        _Transition2.default,
        {
          appear: true,
          'in': this.props.show,
          timeout: 100
        },
        function (state) {
          var _classNames;

          return _react2.default.createElement(
            _reactPortal.Portal,
            { node: document.body },
            _react2.default.createElement(
              'div',
              {
                className: (0, _classNames3.default)((_classNames = {}, _defineProperty(_classNames, jdpClasses, true), _defineProperty(_classNames, 'in', state === 'entered' && _this4.state.visible), _defineProperty(_classNames, 'top', currentPlacement === placements.TOP), _defineProperty(_classNames, 'right', currentPlacement === placements.RIGHT), _defineProperty(_classNames, 'bottom', currentPlacement === placements.BOTTOM), _defineProperty(_classNames, 'left', currentPlacement === placements.LEFT), _classNames)),
                ref: function ref(_ref) {
                  _this4.jdp = _ref;
                },
                onClick: _this4.handleJDPClick
              },
              _react2.default.createElement('div', { className: 'arrow' }),
              _react2.default.createElement(
                'div',
                { className: 'content' },
                _react2.default.createElement(_functionButtons2.default, {
                  month: monthName,
                  year: year,
                  showMonthsTable: _this4.showMonthsTable,
                  showYearsTable: _this4.showYearsTable
                }),
                _react2.default.createElement(_weekHeader2.default, { daysName: type === types.JALAALI ? (0, _dateUtil.mapJalaaliDayNames)() : (0, _dateUtil.mapGregorianDayNames)() }),
                _react2.default.createElement(_calendar3.default, {
                  monthDays: monthDays,
                  pickDay: _this4.pickDay,
                  highlightWeekends: _this4.props.highlightWeekends
                }),
                _react2.default.createElement(_footer2.default, {
                  today: _this4.state.options.today,
                  type: _this4.state.options.type,
                  toggleCalendarType: _this4.toggleCalendarType,
                  pickDay: _this4.pickDay,
                  size: _this4.state.options.size,
                  shape: _this4.state.options.shape
                }),
                _react2.default.createElement(_monthTable2.default, {
                  monthList: type === types.JALAALI ? (0, _dateUtil.mapJalaaliMonths)(month) : (0, _dateUtil.mapGregorianMonths)(month),
                  visible: _this4.state.isMonthsVisible,
                  hideMonthTable: _this4.hideMonthTable,
                  changeMonth: _this4.changeMonth
                }),
                _react2.default.createElement(_yearsTable2.default, {
                  yearsList: (0, _dateUtil.mapYears)(year, _this4.state.options.yearsPage),
                  visible: _this4.state.isYearsVisible,
                  hideYearsTable: _this4.hideYearsTable,
                  changeYear: _this4.changeYear,
                  nextYearsPage: _this4.nextYearsPage,
                  prevYearsPage: _this4.prevYearsPage
                })
              )
            )
          );
        }
      );
    }
  }]);

  return Jdp;
}(_react.Component);

exports.default = Jdp;


Jdp.propTypes = {
  show: _propTypes2.default.bool.isRequired,
  parent: _propTypes2.default.object,
  onHide: _propTypes2.default.func.isRequired,
  onPick: _propTypes2.default.func.isRequired,
  type: _propTypes2.default.oneOf(['jalaali', 'gregorian']),
  size: _propTypes2.default.oneOf(['small', 'medium', 'large']),
  placement: _propTypes2.default.oneOf(['top', 'right', 'bottom', 'left']),
  animated: _propTypes2.default.bool,
  year: _propTypes2.default.number,
  month: _propTypes2.default.number,
  day: _propTypes2.default.number,
  current: _propTypes2.default.object,
  format: _propTypes2.default.string,
  highlightWeekends: _propTypes2.default.bool,
  shape: _propTypes2.default.oneOf(['rectangle', 'square'])
};

Jdp.defaultProps = {
  type: 'jalaali',
  size: 'medium',
  placement: 'bottom',
  animated: true,
  format: 'yyyy/mm/dd',
  highlightWeekends: true,
  shape: 'rectangle'
};