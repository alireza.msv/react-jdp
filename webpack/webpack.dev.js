import webpack from 'webpack';
import path from 'path';
import htmlWebpackPlugin from 'html-webpack-plugin';

import loaders from './webpack.loaders';

loaders.push({
  test: /\.scss$/,
  exclude: ['node_modules'],
  loader: ['css-hot-loader', 'style-loader', 'css-loader?importLoader=1', 'sass-loader']
})

const HOST = process.env.host || 'localhost';
const PORT = process.env.port || 3013;

export default {
  entry: [
    path.join(__dirname, '../dev.jsx')
  ],
  devtool: process.env.WEBPACK_DEVTOOL || 'eval-source-map',
  output: {
    publicPath: '/',
    path: path.join(__dirname, 'public'),
    filename: 'react-jdp.js'
  },
  resolve: {
    extensions: ['.jsx', '.js']
  },
  module: {
    loaders
  },
  devServer: {
    contentBase: './public',
    hot: true,
    inline: true,
    historyApiFallback: true,
    port: PORT,
    host: HOST
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"development"'
      }
    }),
    new htmlWebpackPlugin({
      template: './templates/index.html',
      files: {
        js: ['react-jdp.js']
      }
    })
  ]
}
