export default [
  {
  test: /\.js(x?)$/,
  exclude: /(node_modules)/,
  loader: 'babel-loader'
  }
]
