import webpack from 'webpack';
import path from 'path';
import htmlWebpackPlugin from 'html-webpack-plugin';
import extractTextPlugin from 'extract-text-webpack-plugin';

import loaders from './webpack.loaders';

loaders.push({
  test: /\.scss$/,
  exclude: ['node_modules'],
  loader: extractTextPlugin.extract({
    fallback: 'style-loader',
    use: 'css-loader?importLoader=1&sourceMap!sass-loader?outputStyle=compressed'
  })
});

export default {
  entry: [
    path.join(__dirname, 'src/Jdp.jsx'),
    path.join(__dirname, 'styles/jdp.scss')
  ],
  output: {
    publicPath: '/',
    path: path.join(__dirname, 'dist'),
    filename: 'react-jdp.js'
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  module: {
    loaders
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      warnings: false,
      mangle: true,
      scew_ie8: true,
      drop_console: true,
      drop_debugger: true
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new extractTextPlugin({
      filename: 'jdp.css',
      allChunks: true
    }),
  ]
}
