'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.dateToString = exports.gregorianMonthLength = exports.mapYears = exports.mapGregorianMonths = exports.mapJalaaliMonths = exports.mapGregorianMonthToCalendar = exports.mapJalaaliMonthToCalendar = exports.mapGregorianDayNames = exports.mapJalaaliDayNames = exports.getGregorianDate = exports.gregorianToday = exports.getJalaaliDate = exports.getJalaaliDayOfWeek = exports.jalaaliToday = undefined;

var _jalaaliJs = require('jalaali-js');

var _calendar = require('../constants/calendar');

var jalaaliToday = exports.jalaaliToday = function jalaaliToday() {
  var tdate = new Date();

  return getJalaaliDate(tdate.getFullYear(), tdate.getMonth() + 1, tdate.getDate());
};

var getJalaaliDayOfWeek = exports.getJalaaliDayOfWeek = function getJalaaliDayOfWeek(day) {
  return day + 2 > 6 ? (day + 1) % 7 : day + 1;
};

var getJalaaliDate = exports.getJalaaliDate = function getJalaaliDate(year, month, day) {
  var date = new Date(year, month, day);

  var _toJalaali = (0, _jalaaliJs.toJalaali)(year, month, day),
      jy = _toJalaali.jy,
      jm = _toJalaali.jm,
      jd = _toJalaali.jd;

  var dow = getJalaaliDayOfWeek(date.getDay());

  return {
    year: jy,
    month: jm,
    day: jd,
    monthName: _calendar.jalaaliMonths[jm - 1],
    dow: dow,
    dowName: _calendar.jalaaliWeekDays[dow]
  };
};

var gregorianToday = exports.gregorianToday = function gregorianToday() {
  var gDate = new Date();
  var year = gDate.getFullYear();
  var month = gDate.getMonth() + 1;
  var day = gDate.getDate();
  var dow = gDate.getDay();
  var monthName = _calendar.gregorianMonths[month - 1];

  return {
    year: year,
    month: month,
    day: day,
    dow: dow,
    monthName: monthName
  };
};

var getGregorianDate = exports.getGregorianDate = function getGregorianDate(year, month, day) {
  var _toGregorian = (0, _jalaaliJs.toGregorian)(year, month, day),
      gy = _toGregorian.gy,
      gm = _toGregorian.gm,
      gd = _toGregorian.gd;

  var date = new Date(gy, gm - 1, gd);
  var dow = date.getDay();

  return {
    year: gy,
    month: gm,
    day: gd,
    monthName: _calendar.gregorianMonths[month - 1],
    dow: dow,
    dowName: _calendar.gregorianWeekDays[dow]
  };
};

var mapJalaaliDayNames = exports.mapJalaaliDayNames = function mapJalaaliDayNames() {
  return _calendar.jalaaliWeekDays.map(function (day) {
    return day[0];
  });
};

var mapGregorianDayNames = exports.mapGregorianDayNames = function mapGregorianDayNames() {
  return _calendar.gregorianWeekDays.map(function (day) {
    return day.substring(0, 3);
  });
};

var mapJalaaliMonthToCalendar = exports.mapJalaaliMonthToCalendar = function mapJalaaliMonthToCalendar(year, month) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : { current: { year: 0, month: 0, day: 0 } };

  var monthFirst = getGregorianDate(year, month, 1);
  var monthFirstDow = getJalaaliDayOfWeek(monthFirst.dow);
  var monthLength = (0, _jalaaliJs.jalaaliMonthLength)(year, month);
  var today = jalaaliToday();
  var current = options.current;


  var days = [];

  if (monthFirstDow > 0) {
    var prevMonthLength = void 0;
    if (month === 1) prevMonthLength = (0, _jalaaliJs.jalaaliMonthLength)(year - 1, 12);else prevMonthLength = (0, _jalaaliJs.jalaaliMonthLength)(year, month - 1);

    for (var i = 0; i < monthFirstDow; i++) {
      days.push({
        month: month > 1 ? month - 1 : 12,
        year: month > 1 ? year : year - 1,
        day: prevMonthLength - monthFirstDow + i + 1,
        prevMonth: true,
        dow: (monthFirstDow - i) % 7
      });
    }
  }

  for (var _i = 0; _i < monthLength; _i++) {
    days.push({
      month: month,
      year: year,
      day: _i + 1,
      current: year === current.year && month === current.month && _i + 1 === current.day,
      today: year === today.year && month === today.month && _i + 1 === today.day,
      dow: (monthFirstDow + _i) % 7
    });
  }if (days.length % 7) {
    if (month === 12) {
      month++;
      year++;
    }

    for (var _i2 = 0; _i2 < days.length % 7; _i2++) {
      days.push({
        day: _i2 + 1,
        month: month < 12 ? month + 1 : 1,
        year: month < 12 ? year : year + 1,
        nextMonth: true,
        dow: days[days.length - 1].dow + 1
      });
    }
  }

  return days;
};

var mapGregorianMonthToCalendar = exports.mapGregorianMonthToCalendar = function mapGregorianMonthToCalendar(year, month) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : { current: { year: 0, month: 0, day: 0 } };

  var monthFirst = new Date(year, month - 1, 1);
  var monthFirstDow = monthFirst.getDay();
  var monthLength = gregorianMonthLength(year, month);
  var current = options.current;

  var today = gregorianToday();

  var days = [];

  if (monthFirstDow > 0) {
    var prevMonthLength = void 0;
    if (month === 1) prevMonthLength = gregorianMonthLength(year - 1, 12);else prevMonthLength = gregorianMonthLength(year, month - 1);

    for (var i = 0; i < monthFirstDow; i++) {
      days.push({
        month: month > 1 ? month - 1 : 12,
        year: month > 1 ? year : year - 1,
        day: prevMonthLength - monthFirstDow + i + 1,
        prevMonth: true,
        dow: (monthFirstDow - i) % 7
      });
    }
  }

  for (var _i3 = 0; _i3 < monthLength; _i3++) {
    days.push({
      year: year,
      month: month,
      day: _i3 + 1,
      current: year === current.year && month === current.month && _i3 + 1 === current.day,
      today: year === today.year && month === today.month && _i3 + 1 === today.day,
      dow: (_i3 + monthFirstDow) % 7
    });
  }if (days.length % 7) for (var _i4 = 0; _i4 < days.length % 7; _i4++) {
    days.push({
      day: _i4 + 1,
      month: month < 12 ? month + 1 : 1,
      year: month < 12 ? year : year + 1,
      nextMonth: true,
      dow: days[days.length - 1].dow + 1
    });
  }return days;
};

var mapJalaaliMonths = exports.mapJalaaliMonths = function mapJalaaliMonths(current) {
  return _calendar.jalaaliMonths.map(function (month, index) {
    return {
      name: month,
      index: index,
      disabled: false,
      current: current === index + 1
    };
  });
};

var mapGregorianMonths = exports.mapGregorianMonths = function mapGregorianMonths(current) {
  return _calendar.gregorianMonths.map(function (month, index) {
    return {
      name: month,
      index: index,
      disabled: false,
      current: current === index + 1
    };
  });
};

var mapYears = exports.mapYears = function mapYears(year, yearspage) {
  var yearsPerPage = 16;
  var startYear = year + yearsPerPage * yearspage - 13;
  var years = [];

  for (var i = 0; i < yearsPerPage; i++) {
    years.push({
      year: startYear + i,
      disabled: false,
      current: year === startYear + i
    });
  }

  return years;
};

var gregorianMonthLength = exports.gregorianMonthLength = function gregorianMonthLength(year, month) {
  if (month === 1 || month === 3 || month === 5 || month === 7 || month === 8 || month === 10 || month === 12) return 31;

  if (month === 4 || month === 6 || month === 9 || month === 11) return 30;

  if (year % 4) return 28;else return 29;
};

var dateToString = exports.dateToString = function dateToString(year, month, day, format) {
  if (typeof format === 'undefined') format = 'yyyy/mm/dd';

  var monthStr = month < 10 ? '0' + month.toString() : month.toString();
  var dayStr = day < 10 ? '0' + day.toString() : day.toString();

  var formatted = '';

  if (format.indexOf('yyyy') >= 0) formatted = format.replace('yyyy', year);else if (format.indexOf('yy') >= 0) formatted = format.replace('yy', year.toString().substring(2));

  if (format.indexOf('mm') >= 0) formatted = formatted.replace('mm', monthStr);else if (format.indexOf('m') >= 0) formatted = formatted.replace('m', month);

  if (format.indexOf('dd') >= 0) formatted = formatted.replace('dd', dayStr);else if (format.indexOf('d') >= 0) formatted = formatted.replace('d', day);

  return formatted;
};