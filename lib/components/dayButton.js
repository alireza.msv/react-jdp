'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classNames = require('class-names');

var _classNames2 = _interopRequireDefault(_classNames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DayButton = function (_Component) {
  _inherits(DayButton, _Component);

  function DayButton(props) {
    _classCallCheck(this, DayButton);

    var _this = _possibleConstructorReturn(this, (DayButton.__proto__ || Object.getPrototypeOf(DayButton)).call(this, props));

    _this.handleButtonClick = _this.handleButtonClick.bind(_this);
    return _this;
  }

  _createClass(DayButton, [{
    key: 'handleButtonClick',
    value: function handleButtonClick(e) {
      e.stopPropagation();

      if (!this.props.disabled) this.props.pickDay(this.props.year, this.props.month, this.props.day);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: (0, _classNames2.default)({
            'cell': true,
            'fade': true,
            'in': true,
            'current': this.props.current,
            // 'disabled': this.props.nextMonth || this.props.prevMonth,
            'next': this.props.nextMonth,
            'prev': this.props.prevMonth,
            'today': this.props.today,
            'weekend': this.props.highlightWeekends && this.props.dow === 6
          }), onClick: this.handleButtonClick },
        this.props.day
      );
    }
  }]);

  return DayButton;
}(_react.Component);

exports.default = DayButton;


DayButton.propTypes = {
  day: _propTypes2.default.number.isRequired,
  pickDay: _propTypes2.default.func.isRequired,
  next: _propTypes2.default.bool,
  prev: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  current: _propTypes2.default.bool,
  today: _propTypes2.default.bool
};