'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classNames = require('class-names');

var _classNames2 = _interopRequireDefault(_classNames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _monthButton = require('./monthButton');

var _monthButton2 = _interopRequireDefault(_monthButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MonthTable = function (_Component) {
  _inherits(MonthTable, _Component);

  function MonthTable(props) {
    _classCallCheck(this, MonthTable);

    var _this = _possibleConstructorReturn(this, (MonthTable.__proto__ || Object.getPrototypeOf(MonthTable)).call(this, props));

    _this.renderMonths = _this.renderMonths.bind(_this);
    _this.hideMonthTable = _this.hideMonthTable.bind(_this);
    _this.changeMonth = _this.changeMonth.bind(_this);
    return _this;
  }

  _createClass(MonthTable, [{
    key: 'hideMonthTable',
    value: function hideMonthTable(e) {
      e.stopPropagation();
      this.props.hideMonthTable();
    }
  }, {
    key: 'changeMonth',
    value: function changeMonth(month) {
      this.props.changeMonth(month);
    }
  }, {
    key: 'renderMonths',
    value: function renderMonths() {
      var seasonDivs = [];
      for (var i = 0; i < 4; i++) {
        var monthCells = [];
        for (var j = 0; j < 3; j++) {
          var month = this.props.monthList[j + i * 3];

          monthCells.push(_react2.default.createElement(_monthButton2.default, _extends({}, month, { index: j + i * 3, key: j, onClick: this.changeMonth })));
        }
        seasonDivs.push(_react2.default.createElement(
          'div',
          { className: 'season', key: seasonDivs.length },
          monthCells
        ));
      }

      return seasonDivs;
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: (0, _classNames2.default)({
            'month-select': true,
            'in': this.props.visible
          }), onClick: this.hideMonthTable },
        _react2.default.createElement(
          'div',
          { className: 'months-container' },
          this.renderMonths()
        )
      );
    }
  }]);

  return MonthTable;
}(_react.Component);

exports.default = MonthTable;


MonthTable.propTypes = {
  monthList: _propTypes2.default.array.isRequired,
  hideMonthTable: _propTypes2.default.func.isRequired
};