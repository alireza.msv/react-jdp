'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classNames = require('class-names');

var _classNames2 = _interopRequireDefault(_classNames);

var _calendarTypes = require('../constants/calendarTypes');

var types = _interopRequireWildcard(_calendarTypes);

var _sizes = require('../constants/sizes');

var sizes = _interopRequireWildcard(_sizes);

var _shapes = require('../constants/shapes');

var shapes = _interopRequireWildcard(_shapes);

var _button = require('./button');

var _button2 = _interopRequireDefault(_button);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Footer = function (_Component) {
  _inherits(Footer, _Component);

  function Footer(props) {
    _classCallCheck(this, Footer);

    var _this = _possibleConstructorReturn(this, (Footer.__proto__ || Object.getPrototypeOf(Footer)).call(this, props));

    _this.todayToString = _this.todayToString.bind(_this);
    _this.handlePickToday = _this.handlePickToday.bind(_this);
    return _this;
  }

  _createClass(Footer, [{
    key: 'todayToString',
    value: function todayToString() {
      var _props$today = this.props.today,
          year = _props$today.year,
          monthName = _props$today.monthName,
          day = _props$today.day;


      var prefix = this.props.type === types.JALAALI ? 'امروز ' : 'today';

      if (this.props.size === sizes.SMALL && this.props.shape === shapes.SQUARE) prefix = '';

      return '' + prefix + day + ' ' + monthName + ' ' + year;
    }
  }, {
    key: 'handlePickToday',
    value: function handlePickToday() {
      var _props$today2 = this.props.today,
          day = _props$today2.day,
          month = _props$today2.month,
          year = _props$today2.year;


      this.props.pickDay(year, month, day);
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'footer-btns' },
        _react2.default.createElement(
          'div',
          { className: 'col right' },
          _react2.default.createElement(
            _button2.default,
            { className: 'button-today', onClick: this.handlePickToday },
            this.todayToString()
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'col left' },
          _react2.default.createElement(
            _button2.default,
            { className: 'button-toggle', onClick: this.props.toggleCalendarType },
            this.props.type === types.JALAALI ? 'جلالی' : 'میلادی'
          )
        )
      );
    }
  }]);

  return Footer;
}(_react.Component);

exports.default = Footer;


Footer.propTypes = {
  today: _propTypes2.default.object.isRequired,
  pickDay: _propTypes2.default.func.isRequired,
  toggleCalendarType: _propTypes2.default.func.isRequired
};