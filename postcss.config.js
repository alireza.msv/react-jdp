var autoprefixer = require('autoprefixer');

const AUTOPREFIXER_BROWSERS = [
  "Android 2.3",
  "Android >= 4",
  "Chrome >= 8",
  "Firefox >= 8",
  "Explorer >= 9",
  "iOS >= 7",
  "Opera >= 9",
  "Safari >= 7.1"
];

module.exports = autoprefixer({browsers: AUTOPREFIXER_BROWSERS});
