import React from 'react';
import { render } from 'react-dom';

import './styles/jdp.scss';
import Jdp from './src/jdp';

class Root extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    }

    this.toggleJdp = this.toggleJdp.bind(this);
  }

  toggleJdp(e) {
    e.preventDefault();
    e.stopPropagation();

    this.setState({
      isVisible: !this.state.isVisible,
    })
  }

  render() {
    return (
      <div style={{textAlign: 'center'}}>
        <button ref={ref => {this.toggleBtn = ref}} onClick={this.toggleJdp}>Toggle datepicker</button>
        <Jdp 
          show={this.state.isVisible} 
          parent={this.toggleBtn}
          onHide={() => {this.setState({isVisible: false})}}
          onPick={(date) => {console.log(date)}}
          placement="top"
          format="yy/mm/dd"
          size="medium"
          shape="rectangle"
        />
      </div>
    )
  }
}

render(<Root />, document.getElementById('app'));

if (module.hot) {
  module.hot.accept(Jdp, () => {
    render(<Root />, document.getElementById('app'));
  })
}
