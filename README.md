# React-Jdp

React-Jdp is an open source Jalaali date picker component for React. 

![demo.png](demo/demo.png)

### Installation

```
$ npm install react-jdp
```

or using `yarn`
```
$ yarn add react-jdp
```

### Loading stlyesheets
Don't forget to import Jdp `css` or `scss` filse into your project

`css` file can be found in:
`node_modules\react-jdp\css\jdp.css`

`scss` file can be found in:
`node_modeuls\react-jdp\styles\jdp.scss`

If you choose to use `scss`, you need to compile it to `css` before publishing your project to the web

### Example of use

Import jdp into your page
```js
import Jdp from 'react-jdp'
```

Place Jdp component whereever you want
```js
<Jdp
  show={this.state.showJdp}
  parent={this.jdpToggleBtn}
  onHide={() => {this.setState({showJdp: false})}}
  onPick={date => {console.log(date)}}
/>
```

### Options

Option            | Default           | Description                                                                                  
------------------|-------------------|--------------------------------------------------------------------------------------------
show              | false (required)  | boolean variable indicates Jdp is visible or not      
parent            | -- (required)     | html element that Jdp position binds to
onHide            | (required)        | the function used to hide Jdp. Should change passed show variable to false 
onPick            | (required)        | the function get called when user pick a date
type              | jalaali           | indicates date picker type. value can be `jalaali` or `gregorian`
size              | md                | the size of Jdp, `sm` for small, `md` for medium and `lg` for large
placement         | bottom            | which placement of parent, Jdp should shown.  chooose one of `top`, `right`, `bottom` & `left`
animated          | true              | `true` value enables usings fadeIn, fadeOut and slide transitions
year              | --                | calendar start year
month             | --                | calendar start month
day               | --                | calendar start day
current           | --                | an object of {day, month, day} that indicates current selected date
format            | yyyy/mm/dd        | string format of returned date on onPick method
highlightWeekends | true              | show weekend days with another color
shape             | 'rectangle'       | indicates how Jdp should like. in `reactanle` mode Jdp will be wide and in `square` Jdp will be like an square

#### Options.parent
For definating parent you can use `ref` or `id`.
Here is two sample code

using ref
```jsx
<button ref={ref => {this.jdpToggleBtn = ref}} >
  ...
</button>
```

using id
```jsx
<button id="jdpToggleBtn">
  ...
</button>
```

#### Options.onPick
When user click on a day on calendar or click on today button this method will be called. 
The date passed variable is an object, with both Jalaali and Gregorian values of selected day.
Both Jalaali and Gregorian are objects contains `year`, `month` and `day` value.
For ease of use, formatted string of selected date passed too. Their named as jalaaliString and gregorianString.
You can change the format of date strign by passing an string prop as `format`

Sample date callback result
```js
{
  jalaali: {year: 1396, month: 11, day: 15},
  gregorian: {yaer: 2018, month: 2, day: 4},
  jalaaliString: '1396/11/15',
  gregorianString: '2018/02/04'
}
```

#### Options.shape
Sets how datepicker should be like. in `rectangle` datepicker will be wide rectangle, but in `square` datepicker will be close to an square, not a shape with equal width and height

#### Options.size
Sets datepicker width based on the following table values.

in `rectangle` shaped datepicker:

Size    | Width
--------|--------
sm      | 250px
md      | 350px
lg      | 450px

in `square` shaped datepicker:

Size    | Width
--------|--------
sm      | 220px
md      | 260px
lg      | 310px

#### Options.placement
Sets in which side on parent element, Jdp should be shown.
Jdp automaticaly checks enough space around the element before showing datepicker. So if you choose `top`, `right` or `left` and on the render time, there was't enough space on selected side of parent element, Jdp chande position to `bottom` and show datepicker beneath the element

#### Options.highlightWeekends
With `true` value, weekends (based on type of datepicker) will be shown with another color. In current version this color is `red`